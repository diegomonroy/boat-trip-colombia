<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php if ( ! is_front_page() ) : ?>
					<!--<p class="text-center hide-for-small-only"><img src="<?php get_site_url(); ?>/boat-trip-colombia/wp-content/themes/WordPress-Theme/assets/images/logo_yellow.png"></p>-->
					<h1 class="text-center"><?php the_title(); ?></h1>
					<?php endif; ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->
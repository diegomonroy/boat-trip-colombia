<?php
if ( get_query_var( 'pop_up' ) == 'yes' ) {
	$theme = 'popup';
} else {
	$theme = '';
}
?>
<?php get_header( $theme ); ?>
	<?php get_template_part( 'part', 'single' ); ?>
<?php get_footer( $theme ); ?>
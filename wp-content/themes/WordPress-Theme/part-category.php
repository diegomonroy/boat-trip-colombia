<?php
$style = '';
if ( is_category( array( 'botes-y-yates', 'catamaran', 'lanchas', 'triton', 'tuna', 'veleros', 'yates' ) ) ) :
	$style = 'boat';
endif;
if ( is_category( 'destinos' ) ) :
	$style = 'destination';
endif;
?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<!--<p class="text-center hide-for-small-only"><img src="<?php get_site_url(); ?>/boat-trip-colombia/wp-content/themes/WordPress-Theme/assets/images/logo_yellow.png"></p>-->
				<h1 class="text-center"><?php single_cat_title(); ?></h1>
			</div>
		</div>
		<?php if ( is_category( array( 'botes-y-yates', 'catamaran', 'lanchas', 'triton', 'tuna', 'veleros', 'yates' ) ) ) : ?>
		<div class="row">
			<div class="small-12 columns">
				<?php
				wp_nav_menu(
					array(
						'menu_class' => 'dropdown menu align-center',
						'container' => false,
						'theme_location' => 'boat-menu',
						'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
					)
				);
				?>
			</div>
		</div>
		<?php endif; ?>
		<div class="row">
			<?php
			$currCat = get_category( get_query_var( 'cat' ) );
			$cat_name = $currCat->name;
			$cat_id = get_cat_ID( $cat_name );
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$temp = $wp_query;
			$wp_query = null;
			$wp_query = new WP_Query();
			$wp_query->query( 'showposts=12&post_type=post&paged=' . $paged . '&cat=' . $cat_id );
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
			?>
			<?php /*if ( have_posts() ) : while ( have_posts() ) : the_post();*/ ?>
				<div class="small-12 medium-3 columns">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<p class="text-center"><a data-fancybox data-type="iframe" data-src="<?php the_permalink(); ?>?pop_up=yes" href="javascript:;"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></a></p>
						<h2><a data-fancybox data-type="iframe" data-src="<?php the_permalink(); ?>?pop_up=yes" href="javascript:;"><?php the_title(); ?></a></h2>
						<?php if ( is_category( array( 'botes-y-yates', 'catamaran', 'lanchas', 'triton', 'tuna', 'veleros', 'yates' ) ) ) : ?>
						<p class="text-center"><a data-fancybox data-type="iframe" data-src="<?php the_permalink(); ?>?pop_up=yes" href="javascript:;" class="hollow button">VER MÁS</a></p>
						<?php endif; ?>
					</article>
				</div>
			<?php /*endwhile; endif;*/ ?>
			<?php endwhile; ?>
		</div>
		<div class="row">
			<div class="small-12 columns text-center custom_pagination">
				<?php
				global $wp_query;
				$big = 999999999;
				echo '<div class="paginate-links">';
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'prev_text' => __( '<<' ),
					'next_text' => __( '>>' ),
					'current' => max( 1, get_query_var( 'paged' ) ),
					'total' => $wp_query->max_num_pages
				) );
				echo '</div>';
				?>
			</div>
		</div>
	</section>
<!-- End Content -->
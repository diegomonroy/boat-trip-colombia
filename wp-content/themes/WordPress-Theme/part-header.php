<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-4 small-order-2 medium-2 medium-order-1 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 small-order-4 medium-8 medium-order-2 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-4 small-order-1 medium-1 medium-order-3 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
			<div class="small-4 small-order-3 medium-1 medium-order-4 columns">
				<?php dynamic_sidebar( 'languages' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->